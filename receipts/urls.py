from django.urls import path

from receipts.views import (
    ReceiptListView, ReceiptCreateView, ExpenseCategoryListView,
    AccountListView, ExpenseCategoryCreateView, AccountCreateView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("categories/", ExpenseCategoryListView.as_view(), name="list_categories"),
    path("accounts/", AccountListView.as_view(), name="account"),
    path("categories/create/", ExpenseCategoryCreateView.as_view(), name="expensecategory_create"),
    path("accounts/create/", AccountCreateView.as_view(), name="create_account"),

]
